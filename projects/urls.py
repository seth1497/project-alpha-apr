from django.urls import path
from projects.views import (
    projects_list_view,
    project_detail_view,
    project_create,
)

urlpatterns = [
    path("", projects_list_view, name="list_projects"),
    path("<int:id>/", project_detail_view, name="show_project"),
    path("create/", project_create, name="create_project"),
]
